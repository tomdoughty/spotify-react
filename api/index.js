const express = require("express");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const routes = require("./routes");

const app = express();

app.use(cookieParser())
	.use(bodyParser.json())
	.use(bodyParser.urlencoded({ extended: false }))
	.use('/', routes);

app.listen(3001, () => {
	console.log('Express server listening on port 3001');
});
