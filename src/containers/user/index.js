import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import { setTokens, getMyInfo, getPlaylists } from '../../actions/spotify'

class User extends React.Component {
    constructor(props) {
        super(props);
        this.getPlaylists = this.getPlaylists.bind(this);
    }

    componentDidMount() {
        const { accessToken, refreshToken } = this.props.match.params;
        this.props.setTokens({accessToken, refreshToken});
        this.props.getMyInfo();
    }

    getPlaylists() {
        this.props.getPlaylists(this.props.user.id);
    }

    render() {
        const { display_name, id, email } = this.props.user;
        return (
            <div>
                { this.props.user &&
                    <div className="user">
                        <h1>{`Logged in as ${display_name}`}</h1>
						<p>Id: {id}</p>
						<p>Email: {email}</p>
                        <button onClick={this.getPlaylists}>{this.props.playlists.loading ? 'Loading...' : 'Get Playlists'}</button>
                        <ul>
                            { Object.values(this.props.playlists.items).map((playlist, index) => (
                                <li key={index}>
                                    {playlist.name}    
                                </li>
                            ))}
                        </ul>
                    </div>
                }
            </div>
        );
    }
}


const mapStateToProps = state => ({
    user: state.spotify.user,
	playlists: state.spotify.playlists
})

const mapDispatchToProps = dispatch => bindActionCreators({
    setTokens,
    getMyInfo,
    getPlaylists
}, dispatch)

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(User)
);