import {
	SPOTIFY_TOKENS,
	SPOTIFY_ME_BEGIN,
	SPOTIFY_ME_SUCCESS,
	SPOTIFY_ME_FAILURE,
	SPOTIFY_PLAYLISTS_BEGIN,
	SPOTIFY_PLAYLISTS_SUCCESS,
	SPOTIFY_PLAYLISTS_FAILURE
} from "../actions/spotify";

const initialState = {
	accessToken: null,
	refreshToken: null,
	user: {
		loading: false,
		display_name: null,
		id: null
	},
	playlists: {
		items: {},
		loading: false
	}
};

export default function reduce(state = initialState, action) {
	switch (action.type) {
		case SPOTIFY_TOKENS:
			const { accessToken, refreshToken } = action;
			return Object.assign({}, state, { accessToken, refreshToken });

		case SPOTIFY_ME_BEGIN:
			return Object.assign({}, state, {
				user: Object.assign({}, state.user, { loading: true })
			});

		case SPOTIFY_ME_SUCCESS:
			return Object.assign({}, state, {
				user: Object.assign({}, state.user, action.data, { loading: false })
			});

		case SPOTIFY_ME_FAILURE:
			return state;

		case SPOTIFY_PLAYLISTS_BEGIN:
			return Object.assign({}, state, {
				playlists: Object.assign({}, state.playlists, { loading: true })
			});

		case SPOTIFY_PLAYLISTS_SUCCESS:
			return Object.assign({}, state, {
				playlists: Object.assign({}, state.playlists, action.data, { loading: false })
			});

		case SPOTIFY_PLAYLISTS_FAILURE:
			return state;

		default:
			return state;
	}
}
