import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
import spotify  from './spotify'

export default combineReducers({
    router: routerReducer,
    spotify
})
